% Esta funci�n realiza de manera recursiva el crecimiento de una regi�n de la imagen
% partiendo de una semilla en (x,y). El crecimiento se hace sobre sus ocho-vecinos.
%
% J. Gonzalez - Mayo 2005
function crec_recur(x,y)
% Variables globales para no pasar tantos argumentos a la funcion
global region; % Region segmentada. Es una matriz del tama?o la imagen,con
% todo a cero salvo la region, a uno.
global media; % Media (dinamica) de la region que va creciendo.
global points_in_region; % Numero de puntos actual de la region segmentada
global im1; % Imagen de entrada en escala de grises
global t; % Desviacion del nivel de gris sobre la media para a?adir
% nuevos pixeles
% Comprobacion que no estamos fuera de la imagen
if (x <= 0 | x > size(im1,2) | y <= 0 | y > size(im1,1) )
return;
end
if (region(x,y) == 1) % si esta ya marcado (=1) no hacemos nada y salimos de la funcion
return;
end
if abs(double(im1(y,x)) - media) < t % Condicion de anadir pixel
region(x,y) = 1;    % tu jest blad z inicjalizacja tam gdzie X i Y = 1
points_in_region = points_in_region + 1;
media = (media  + (double(im1(y,x))/2)); % No necesitamos calcular media,
%porque nuestro soluci�n estar� mal.
%calculo dinamico de la media
% Recursion sobre los 8-vecinos
plot(x,y,'g');
crec_recur(x-1,y+1);
crec_recur(x-1,y);
crec_recur(x-1,y-1);
crec_recur(x,y+1);
crec_recur(x,y-1);
crec_recur(x+1,y+1);
crec_recur(x+1,y);
crec_recur(x+1,y-1);
end
return;