im = imread('torre_monica.TIF');        
figure, subplot(2,2,1) 
imshow(im)
title('Imagen original')
nPixels = prod(size(im));
data = reshape(im, 1,nPixels ); %Image as a vector
pert = kmeans(double(data), 3); %3 - para RGB 
clus=reshape(pert, size(im)); %Vector Image back to a matrix
im_clust=uint8(255*clus/max(max(max(clus)))); % - si es RGB pues necesitamos 3xMax
subplot(2,2,2), imshow(im_clust), title('Imagen segmentada')

im = rgb2gray(im);
subplot(2,2,3) 
imshow(im);
title('Imagen gris')
nPixels = prod(size(im));
data = reshape(im, 1,nPixels ); %Image as a vector
pert = kmeans(double(data), 2); %2 - para Gris 
clus=reshape(pert, size(im)); %Vector Image back to a matrix
im_clust=uint8(255*clus/max(max(clus)));
subplot(2,2,4)
imshow(im_clust),title('Imagen segmentada');