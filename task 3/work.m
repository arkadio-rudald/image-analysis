global im1;
global region; % Region segmentada. Es una matriz del tama?o la imagen,con
global t;

im1 = imread('rice.tif');
[size_x, size_y] = size(im1);
t = 25;
region = zeros(size_x,size_y);
global media; % Media (dinamica) de la region que va creciendo.
global points_in_region; % Numero de puntos actual de la region segmentada
points_in_region = 0;

%Casos inicios
subplot(1,2,1);
imshow(im1);
[x,y] = ginput(1);
x = uint8(x);
y = uint8(y);
region (x, y) = 0;
media = double(im1(y,x));                


subplot(1,2,2);
imshow(im1);
hold on;
crec_recur(x,y);

%Colorear border de imagen segmentada
for x=1:size_x
    for y=1:size_y
    
        if(region(x,y) == 1)
            if(region(x+1,y+1) ==0)
                plot(x,y,'g');
            end
            if(region(x+1,y) ==0)
                plot(x,y,'g');
            end
            if(region(x+1,y-1) ==0)
                plot(x,y,'g');
            end
            if(region(x-1,y+1) ==0)
                plot(x,y,'g');
            end
            if(region(x-1,y) ==0)
                plot(x,y,'g');
            end
            if(region(x-1,y-1) ==0)
                plot(x,y,'g');
            end
            if(region(x,y+1) ==0)
                plot(x,y,'g');
            end
            if(region(x,y-1) ==0)
                plot(x,y,'g');
            end
            
        end
    
    end
end
