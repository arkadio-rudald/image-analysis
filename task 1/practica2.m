function pa = practica2(imagen, umbral)

im = edge(imagen, 'sobel', umbral, 'horizontal');
im2 = edge(imagen, 'sobel', umbral, 'vertical');

suma = im + im2;

Imbinaria = (im | im2).*255;

IMG2(:,:,1) = im;
IMG2(:,:,2) = double(im) * double(Imbinaria(1:end, 1:end));
IMG2(:,:,3) = im;
		      
subplot(2,2,1);
imshow(imagen);
title('Imagen original');
subplot(2,2,2);
imshow(im);
title('Sobel horizontal');
subplot(2,2,3);
imshow(im2);
title('Sobel vertical');
subplot(2,2,4);
imshow(IMG2);
title('Suma de Sobel');

pa = suma;