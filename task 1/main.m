imagen = imread('rice.tif');
%practica2(image, 0.04);
umbral = 0.02;

im = edge(imagen, 'sobel', umbral, 'horizontal');
im2 = edge(imagen, 'sobel', umbral, 'vertical');

suma = im + im2;

Imbinaria = (im | im2).*255;

IMG2(:,:,1) = im;
IMG2(:,:,2) = double(im) + double(Imbinaria(1:end, 1:end));
IMG2(:,:,3) = im;
		      
IMG2 = im2uint8(IMG2);

subplot(2,2,1);
imshow(imagen);
title('Imagen original');
subplot(2,2,2);
imshow(im);
title('Sobel horizontal');
subplot(2,2,3);
imshow(im2);
title('Sobel vertical');
subplot(2,2,4);
imshow(IMG2);
title('Suma de Sobel');

pa = suma;