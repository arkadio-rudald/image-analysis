function y = efecto_Canny(nombre_imagen);
		
	 im = imread(nombre_imagen);
	 i = 1;
	 for sigma = 0.5:0.3:1.5
 	 for thresh = 0.01:0.03:0.09
	 subplot(4,4,i)
	 edge(im,'canny',thresh,sigma)
	 title(sprintf('thresh: %f, sigma %f ', thresh, sigma));
	 i=i+1;
	 end;
	 end;
