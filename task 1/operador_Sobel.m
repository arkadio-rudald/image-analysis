function  [angulo_borde] = operador_Sobel(nombre_imagen, umbral);
             	      
		      if nargin < 2, umbral = 50;, end;
		
		      im = imread(nombre_imagen);
		      Hf = fspecial('sobel');
		      Hc = Hf';
		      Gh = conv2(double(im), Hf);
		      Gv = conv2(double(im), Hc);
		      dir_gradiente = atan2(Gh,Gv);
		      [angulo_borde] = (dir_gradiente(2:end-1, 2:end-1));
		      Imh = (Gh > umbral).*255;
		      Imv = (Gv > umbral).*255;
		      Imbinaria = (Imh | Imv).*255;
		      ImG(:,:,1) = im;
		      ImG(:,:,2) = double(im) + double(Imbinaria(2:end-1, 2:end-1));
		      ImG(:,:,3) = im;

		      subplot(2,2,1), imshow(im), title('Imagen Original');
		      subplot(2,2,2), imshow(Imh), title('Imagen Sobel Horizontal');
		      subplot(2,2,3), imshow(Imv), title('Imagen Sobel Vertical');
		      subplot(2,2,4), imshow(ImG), title('Imagen de bordes (verde) sobre la original');
		      
