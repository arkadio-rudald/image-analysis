% Cogo 2 primeros momentos de Hu de 15 imagenes A
a = 'imagenes_botellas/botella_A_';  
c = '.bmp';
HM1 = zeros(15,2);
HM2 = zeros(15,2);
HM3 = zeros(15,2);
for i=1:15
    b = num2str(i);
    d = strcat(a,b,c);
    I = imread(d);
    HM1a = momentos_Hu(I);
    for j=1:2
        HM1(i,j) = HM1a(1,j);   %Menos acercamiento
    end
end


% Cogo 2 primeros momentos de Hu de 15 imagenes B
a = 'imagenes_botellas/botella_B_';  
c = '.bmp';

for i=1:15
    b = num2str(i);
    d = strcat(a,b,c);
    I = imread(d);
    HM2a = momentos_Hu(I);
    for j=1:2
        HM2(i,j) = HM2a(1,j);   %Menos acercamiento
    end
    
end

% Cogo 2 primeros momentos de Hu de 15 imagenes C
a = 'imagenes_botellas/botella_C_';  
c = '.bmp';

for i=1:15
    b = num2str(i);
    d = strcat(a,b,c);
    I = imread(d);
    HM3a = momentos_Hu(I);
    for j=1:2
        HM3(i,j) = HM3a(1,j);   %Menos acercamiento
    end
end

%Pinto graph
figure
hold on;
media1a = 0;
media1b = 0;
media2a = 0;
media2b = 0;
media3a = 0;
media3b = 0;
for i=1:15
    okrag(HM1(i,1),HM1(i,2),1,'r');
    okrag(HM2(i,1),HM2(i,2),1,'g');
    okrag(HM3(i,1),HM3(i,2),1,'b');
    
    media1a = HM1(i,1) + media1a;
    media1b = HM1(i,2) + media1b;
    media2a = HM2(i,1) + media2a;
    media2b = HM2(i,1) + media2b;
    media3a = HM3(i,1) + media3a;
    media3b = HM3(i,1) + media3b;
end

media1a = media1a/15;
media1b = media1b/15;
media2a = media2a/15;
media2b = media2b/15;
media3a = media3a/15;
media3b = media3b/15;

okrag(media1a, media1b,3,'r');
okrag(media2a, media2b,3,'g');
okrag(media3a, media3b,3,'b');