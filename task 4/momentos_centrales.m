function [mu00,mu10,mu01,mu11,mu20,mu02,mu21,mu12,mu30,mu03] = momentos_centrales(I)
[size_x,size_y] = size(I);
M00 = 0;
M10 = 0;
M01 = 0;
M20 = 0;
M02 = 0;
M11 = 0;
M30 = 0;
M03 = 0;
M21 = 0; 
M12 = 0;
for x=1:size_x
    for y=1:size_y
        M00 = double(I(x,y)) + M00;
        M10 = (x * double(I(x,y))) + M10;
        M01 = (y * double(I(x,y))) + M01;
     
        M20 = (x*x * double(I(x,y))) + M20;
        M11 = (x* double(I(x,y))) + (y * double(I(x,y))) + M20;
        M02 = (y * y * double(I(x,y))) + M20;
        
        M30 = (x * x * x * double(I(x,y))) + M30;
        M21 = (x * x * y * double(I(x,y))) + M21;
        M12 = (x * y * y * double(I(x,y))) + M12;
        M03 = (y * y * y * double(I(x,y))) + M03;
    end
end

x_sr = (M10/M00);
y_sr = (M01/M00);

U = M00;
U01a = 0; 
U10a = 0;
U20a = M20 - (U * (x_sr^2));
U11a = M11 - (U * x_sr * y_sr);
U02a = M02 - (U * (y_sr^2));
U30a = M30 - (3 * M20 * x_sr) + (2 * U * (x_sr^3));
U21a = M21 - (M20 * y_sr) - (2 * M11 * x_sr) + (2 * U * (x_sr^2) * y_sr);
U12a = M12 - (M02 * x_sr) - (2 * M11 * y_sr) + (2 * U * x_sr * (y_sr^2));
U03a = M03 - (3 * M02 * y_sr) + (2 * U * (y_sr^3));

mu00 = M00;
mu01 = U01a;
mu10 = U10a;
mu20 = U20a;
mu11 = U11a;
mu02 = U02a;
mu30 = U30a;
mu21 = U21a;
mu12 = U12a;
mu03 = U03a;

% U10 = 0;
% U01 = 0;
% U20 = 0;
% U11 = 0;
% U02 = 0;
% U30 = 0;
% U21 = 0;
% U12 = 0;
% U03 = 0;
% 
% for x=1:size_x
%     for y=1:size_y
%         U10 = (x - x_sr)*((y - y_sr)^0)*double(I(x,y)) + U10;
%         U01 = ((x - x_sr)^0)*(y - y_sr)*double(I(x,y)) + U01;
%         
%         U20 = ((x - x_sr)^2)*((y - y_sr)^0)*double(I(x,y)) + U20;
%         U11 = ((x - x_sr) * (y - y_sr) * double(I(x,y))) + U11;
%         U02 = ((x - x_sr)^0)*((y - y_sr)^2)*double(I(x,y)) + U02;
%         
%         U30 = ((x - x_sr)^3)*((y - y_sr)^0)*double(I(x,y)) + U30;
%         U21 = ((x - x_sr)^2)*((y - y_sr))*double(I(x,y)) + U21;
%         U12 = ((x - x_sr))*((y - y_sr)^2)*double(I(x,y)) + U12;
%         U03 = ((x - x_sr)^0)*((y - y_sr)^3)*double(I(x,y)) + U03;
%      end
% end
% MOMENTOS ORDEN 2