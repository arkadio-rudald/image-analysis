function fin = circulos(imagen, uvotos, Rmin, Rmax,R)

imagen = im2double(imagen);         %convercion a double
[Fx,Fy] = gradient(imagen);         %calcular gradient
im_gradient = abs(Fx) + abs(Fy);    %


binar = im2bw(im_gradient,0.1);     %binarizacion de imagen

[size_x, size_y] = size(binar);     %
subplot(2,2,1);
imshow(imagen); 
title('Imagen original');
subplot(2,2,2);
imshow(im_gradient);                %gradient
title('Gradient');
subplot(2,2,3);                     
imshow(binar);                      %binaryzacion de imagen
title('Binarizacion de imagen');
subplot (2,2,4);
image (imagen);                     
axis image;
xlabel('Circulos detectadas')
hold on;



Akumulator = zeros(size_x,size_y);          %Acumulator 
theta = zeros(size_x,size_y);               

for y = 1:size_y
    for x = 1:size_x
        for R1=Rmin:1:Rmax
            if(binar(x,y))
                
                theta(x,y) = atan2(Fy(x,y),Fx(x,y));
                a1 = y + R1*cos(theta(x,y));
                a2 = y - R1*cos(theta(x,y));
                
                b1 = x + R1*sin(theta(x,y));
                b2 = x - R1*sin(theta(x,y));
                
                a1 = uint8(a1); % INT8  0 - 127
                a2 = uint8(a2); % UINT8 0 - 255
                b1 = uint8(b1);
                b2 = uint8(b2);
                
                
                if(a1>0 && a1<size_y && b1>0 && b1<size_x)      
                    Akumulator(b1,a1) = Akumulator(b1,a1) + 1;
                end
                if(a2>0 && a2<size_y && b2>0 && b2<size_x)      
                    Akumulator(b2,a2) = Akumulator(b2,a2) + 1;
                    
                end
            end
        end
   end
end

% Buscamos maximos
% Revisamos vecinos para reducir circulos erroneos
for y = 1:size_y
    for x = 1:size_x    
         if(Akumulator(x,y) > uvotos)
              if(Akumulator(x,y) > Akumulator(x-1,y+1))
                  if(Akumulator(x,y) > Akumulator(x-1,y))
                      if (Akumulator(x,y) > Akumulator(x-1,y-1))
                            if(Akumulator(x,y) > Akumulator(x,y+1))
                                    if (Akumulator(x,y) > Akumulator(x,y-1))
                                        if(Akumulator(x,y) > Akumulator(x+1,y+1))
                                            if (Akumulator(x,y) > Akumulator(x+1,y))
                                                if(Akumulator(x,y) > Akumulator(x+1,y-1))
                                                    for i=-pi:0.1:pi
                                                        a = x + R*cos(i);
                                                        b = y + R*sin(i);
                                                        plot(b,a,'r');
                                                    end
                                                end
                                            end
                                        end
                                    end
                                
                            end
                      end
                      
                  end
              end
         end
    end
end
fin = a;
